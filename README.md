# Desafio final Dio.me / Banco Carrefour

## Objetivo:
Criar um pipeline de deploy desta aplicação em forma de containers em um cluster Kubernetes.
<br>O participante poderá utilizar um cluster kubernetes em nuvem (Preferencialmente utilizando
GCP).
<br>O pipeline de CI/CD poderá ser criado utilizando-se o Gitlab, Terraform ou outra solução desejada.

Este repositório contém:
* os arquivos da aplicação "Cadastro de usuários", nas pastas backend, db e frontend;
* os arquivos de geração das imagens docker (dockerfile):
  * na pasta backend: para gerar a imagem da aplicação em php;
  * na pasta db: para gerar a imagem do banco de dados MySQL;

## Imagens docker:
As imagens geradas estão nos repositórios:
* Banco de dados: szalbuque/cadastro-db-desafio:1.0
* Backend: szalbuque/cadastro-app-php:1.0

## Frontend:
* Uma página simples, usando html+css, com um botão que direciona para a aplicação backend
* Depois de criado o Load Balancer, no arquivo **[index.html](frontend/index.html)**, é necessário incluir **IP externo** do Load Balancer na linha:
  > window.open("http://localhost:8081");
* Esta página pode ser aberta em qualquer navegador, para acessar a aplicação.

# Cluster Kubernetes no Google Cloud
## Criação do cluster GKE
* Especificações do cluster

Item | Descrição
-----|----------
Name | cluster-app-cadastro	
Location type | Zonal	
Control plane zone | southamerica-east1-a	
Total size | 3	
Endpoint | 34.95.191.7

## Ativar o uso do Filestore pelo Cluster:
>  gcloud container clusters update cluster-app-cadastro --zone southamerica-east1-a --project bootcampdiome --update-addons=GcpFilestoreCsiDriver=ENABLED

## Criação do volume persistente para o banco de dados
* Especificações do volume Filestore (NFS):

Instance ID | File share name | Creation time | Service tier | Location | IP address | Capacity | Labels
------------|-----------------|---------------|--------------|----------|------------|----------|-------
gcpnfs | dados | 26.11.2022, 15:31:32 | BASIC_HDD | southamerica-east1-a | 172.31.43.194 | 1 TiB	

* Arquivo **[persistentvolume.yml](persistentvolume.yml)**:
  * Especificação do Persistent Volume, que faz a ligação com o compartilhamento NFS:  **172.31.43.194/dados**

* Arquivo **[pvc.yml](pvc.yml)**:
  * Especificação do Persistent Volume Claim, com o nome de **mysql-dados**

## Verificação do volume persistente:
> kubectl describe pv


Item | Descrição
-----|----------
Name: | fileserver
Status: | Bound
Claim: | default/mysql-dados
Reclaim Policy: | Retain
Access Modes: | RWX
VolumeMode: | Filesystem
Capacity: | 1T
Type: | NFS (an NFS mount that lasts the lifetime of a pod)
Server: | 10.171.253.154
Path: | /dados
ReadOnly: | false

> kubectl describe pvc

Item | Descrição
-----|----------
Name: | mysql-dados
Namespace: | default
Status: | Bound
Volume: | fileserver
Annotations: |  pv.kubernetes.io/bind-completed: yes
Capacity:   |   1T
Access Modes: | RWX
VolumeMode:  |  Filesystem
Used By:    |   mysql-64fb768769-rndqm

## Implantação do container mysql
* Deploy:
> kubectl apply -f [dbasedeploy.yml](dbasedeploy.yml)
* Ver o pod:
> kubectl get pods

NAME | READY | STATUS | RESTARTS | AGE
-----|-------|--------|----------|----
mysql-64fb768769-qs2qr |  1/1  |   Running |  0     |     10m

* Inspecionar o pod:
> kubectl describe pod mysql-64fb768769-qs2qr

Name:             mysql-64fb768769-qs2qr<br>
Namespace:        default<br>
Node:             gke-cluster-app-cadastro-default-pool-5c805f52-3qqv/10.158.0.16<br>
Start Time:       Sat, 26 Nov 2022 18:48:36 -0300<br>
Labels:           app=mysql<br>
Status:           Running<br>
IP:               10.100.1.6<br>
Containers:<br>
  mysql:<br>
    Image:         szalbuque/cadastro-db-desafio:1.0<br>
    Port:          3306/TCP<br>
    Mounts:<br>
      /var/lib/mysql from mysql-dados (rw)<br>
Volumes:<br>
  mysql-dados:<br>
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)<br>
    ClaimName:  mysql-dados<br>
    ReadOnly:   false<br>

## Implantação do container da aplicação backend (php)
* Na pasta backend, onde está o arquivo [deployment.yml](backend/deployment.yml), rodar o comando:
> kubectl apply -f deployment.yml
> kubectl get pods

NAME | READY | STATUS | RESTARTS | AGE
-----|-------|--------|----------|----
mysql-64fb768769-qs2qr |  1/1  |   Running |  0     |     10m
php-546ccdcb77-p8qsq  |   1/1  |   Running  | 0     |     95s

* Acessar o pod da aplicação para corrigir o IP do container MySQL no arquivo config.php:
> kubectl exec --tty --stdin php-546ccdcb77-p8qsq bash

## Implantação dos serviços: Load Balancer e MySQL-connection
> kubectl apply -f [services.yml](services.yml)
> kubectl get service

NAME       |        TYPE     |      CLUSTER-IP  |   EXTERNAL-IP   |   PORT(S)   |     AGE
-----------|-----------------|------------------|-----------------|-------------|--------
kubernetes |        ClusterIP  |    10.104.0.1   |  <none>       |    443/TCP   |     4h9m
mysql-connection  | ClusterIP  |    None         |  <none>       |    3306/TCP  |     97s
php-lb           |  LoadBalancer |  10.104.4.152 |  34.151.233.102 |  80:30855/TCP |  97s
<br>
<br>
# Configuração do pipeline CI/CD com GitLab
-------------------------------------------

## Preparação do repositório no GitLab:
* Criação do repositório https://gitlab.com/szalbuque/dio_carrefour_desafio_final_cicd
* Imagem base da aplicação: szalbuque/cadastro-app-php:1.0
* Pasta da aplicação: /backend
* Arquivo de geração do pipeline: [.gitlab-ci.yml](.gitlab-ci.yml)
* Variáveis para login no docker hub: **REGISTRY_USER** e **REGISTRY_PASS** (necessário configurar no GitLab)
* Variável com a chave privada para acesso ao **bastion-host**: SSH_KEY
* Variável com o IP externo do **bastion-host**: SSH_SERVER
* Configuração das variáveis no GitLab: no repositório, acessar Settins > CI/CD > Variables
* Fazer o push para o GitLab
* Entrar no repositório do GitLab, em CI/CD para verificar a execução do job
* OBSERVAÇÃO: Ao usar variáveis protegidas, as branches também precisam ser protegidas. A branch main é protegida por default.
  * se for criar outras branches para fazer os testes, proteger essas branches em Settings > Repositories > Protected branches

## Criação da máquina Bastion Host:
* Uma máquina virtual que pode ser criada por IaC, mas neste exercício será uma máquina virtual ubuntu criada manualmente no GCP
* Esta VM precisa ter acesso à mesma rede do Cluster
* Preferencialmente, na mesma região do Cluster

### Bastion-host:
Item | Descrição
-----|----------
Name | bastion-host
Creation time | Nov 27, 2022, 10:59:54 AM UTC-03:00
Zone | southamerica-east1-a
Network | default
Internal IP | 10.158.0.18
External IP | 34.95.239.46

### Criação do par de chaves para acesso ao Bastion-host:
* Para usuários do Windows, é mais fácil gerar as chaves usando o PuTTY e o PuTTYgen
* No console GCP, entrar na VM e clicar em Editar
* Em Segurança e Acesso > Chaves SSH, criada chave pública com usuário **gcp**

### Preparação do bastion-host
* Acessar o bastion-host pelo PuTTY, para prepará-lo
* Precisa:
  * instalar gcloud
  * instalar git
  * configurar o usuário de login no GCP
  * testar o acesso ao cluster (realizar as instalações requeridas pelo sistema)
  * ao logar, o sistema estará no diretório: **/home/gcp**

